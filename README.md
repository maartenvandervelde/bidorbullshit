# **Bid or Bullsh!t** #
### A game of wit and deception for the iPad with a built-in ACT-R opponent. ###

Created for the course Cognitive Modelling: Complex Behaviour (April 2017).

![bidorbullshit.png](https://bitbucket.org/repo/z8jxRpr/images/2598546214-bidorbullshit.png)